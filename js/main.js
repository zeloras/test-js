let Regul = {
    numsList: [0,0,0],
    config: {
        first_a_min_max: [6,9], //Range for first input
        sum_min_max: [11,14], //Range for last input
        main_regul: ['.main-expr__input_first', '.main-expr__input_second'], // text fields
        segments: ['.main-expr-line-arcs__item_first', '.main-expr-line-arcs__item_second', '.main-expr__input_equal'], //all inputs
        segment_line: '.main-expr-line-arcs__line', //segment item
        segment_width: 32, //width for one cell on sprite
        segment_left: 27 //indent
    },
    
    init: function () {
        let $self = this;
        let segment_array = $self.config.segments;
        /**
         * Get numbers for segment
         */
        $self.getNumbers($self.config.first_a_min_max,$self.config.sum_min_max);

        /**
         * Set numbers for segment and expression
         */
        $self.initRegul($self.numsList);
        
        /**
         *  Set binds for all inputs
         */
        
        for (let segment in segment_array) {
            let input = $(segment_array[segment]);
            let isinput = false;
            if ($self.numsList.length - 1 !== Number(segment)) {
                input = input.find("input");
            } else {
                isinput = true;
            }

            input.on("keyup", function () {
                $self.checkNumber(this.value, segment, isinput);
            });
        }
    },
    
    /**
     * Check if number right and add or remove input state
     * If check Ok, then call method for show next block and clear input
     * @param {number} number
     * @param {number} segmentNum
     */
    checkNumber: function(number, segmentNum, isInput) {
        let $self = this;
        let element_input = $($self.config.segments[segmentNum]).find("input");
        let element_text = $($self.config.main_regul[segmentNum]);

        if (isInput) {
            element_input = $($self.config.segments[segmentNum]);
        }

        if (Number(number) === Number($self.numsList[segmentNum])) {
            element_input.removeClass('errorClass');
            element_text.removeClass('errorClass');
            $self.inputCheck(element_input, segmentNum);
        } else {
            element_input.addClass('errorClass');
            element_text.addClass('errorClass');
        }
    },
    
    /**
     * Remove input and show next segment
     * Disable current input and focus on next element
     * @param {object} element
     * @param {number} segment
     */
    inputCheck: function(element, segment) {
            let $self = this;
            let $segment = Number(segment) + 1;
            let $elem = $($self.config.segments[$segment]);
            element.attr("disabled", "disabled").addClass("emptyInput");

            if ($elem !== undefined) {
                $elem.css('display', 'inline-block');
                if ($elem.hasClass("emptyInput")) {
                    $elem.removeClass("emptyInput").removeAttr("disabled").val(null).focus();
                } else {
                    $elem.find("input").focus();
                }
            }
    },
    
    /**
     * Set numbers for NumList var
     * @param {array} nums
     * @param {array} summ
     */
    getNumbers: function (nums, summ) {
        let $self = this;
        let aNum = $self.getRandom(nums[0], nums[1]);
        let maxSum = $self.getRandom(summ[0], summ[1]);
        $self.numsList = [
            aNum,
            maxSum - aNum,
            maxSum
        ];
    },
    
    /**
     * Set segment width and set numbers for expression
     * 
     * @param {array} numlist
     */
    initRegul: function (numlist) {
        let $self = this;
        let second_segment = Number(numlist[1]) * Number($self.config.segment_width);
        let first_segment = Number(numlist[0]) * Number($self.config.segment_width);
        /**
         * set nums for main expression
         */
        $($self.config.main_regul[0]).text(numlist[0]);
        $($self.config.main_regul[1]).text(numlist[1]);
        /**
         * set segment width and indent
         * -7px for second indent segment
         */
        $($self.config.segments[0]).width(first_segment + $self.config.segment_left);
        $($self.config.segments[1]).width(second_segment + $self.config.segment_left);

        $($self.config.segments[0]).find($self.config.segment_line).width(first_segment);
        $($self.config.segments[1]).find($self.config.segment_line).width(second_segment);
        $($self.config.segments[1]).css('left', (first_segment - 7) +"px");
    },
    
    /**
     * Get random number
     * @param {Number} min
     * @param {Number} max
     * @returns {Number}
     */
    getRandom: function (min, max) {
        return Math.floor(Math.random() * ((max+1) - min)) + min;
    }
};

Regul.init();